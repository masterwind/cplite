// ==UserScript==
// @name         CP lite
// @description  Функционал панели управления сайта
// @namespace    cp-lite-masterwind
// @author       masterwind
// @version      25.2.6
// @updateURL    https://cplite.netlify.app/cpLITE.user.js
// @downloadURL  https://cplite.netlify.app/cpLITE.user.js
// @supportURL   https://bitbucket.org/masterwind/cplite/issues/
// @match        */tmpls/*
// @match        */panel/
// @match        */panel?*
// @match        */panel/?*
// @icon         https://cplite.netlify.app/logo.png
// @require      https://utils-masterwind.netlify.app/utils.js
// @grant        none
// @noframes
// ==/UserScript==

/* eslint-disable no-multi-spaces */

;((cpl, DATE, D, M, Y, stp, tpl, utils) => {
	stp.DEBUG && console.log(cpl, DATE, D, M, Y, stp, tpl);

	// prepare setup
	self.stp = Object.assign(self.stp || {}, stp);

	// exit if page not assigned
	if (/=fm/.test(location.search) && /file=/.test(location.search)) return;

	self.cplURL = new URLSearchParams(location.search.replace(/;/g, '&'));
	let action = self.cplURL.get('a') || 'cp';

	switch (action) {
		case 'tt' :
		case 'smiles' :
		case 'moders' :
			return; break;

		case 'pro' : break;

		// default :
	}

	// set css
	// && utils.css.setLink('https://cplite.netlify.app/style.css?v=1631469620317')  // set styles

	// TODO - FIXME



	//// фокус на кнопке "активировать модуль" и автосабмит
stp.DEBUG && console.log(`window.frm663`, !!window.frm663 );
	window.frm663 && window.subbutfrm663.focus();  // фокус на кнопке активации
	window.frm663 && (utils.location.has('?a=stuff') || utils.location.has('?a=video'))  // для модулей (игры|видео) отключим контент
		&& window.sg0 && window.sg0.setAttribute('checked', true);
	window.frm663 && !(utils.location.has('?a=shop') || utils.location.has('?a=board') || utils.location.has('?a=stuff') || utils.location.has('?a=video') || utils.location.has('?a=dir'))  // если модуль не (магазин|объявления|игры|видео|каталог сайтов)
		&& stp['autoenableMODULE'] && setTimeout("window.frm663.submit()", 500);


	//// функционал в общих настройках сайта
stp.DEBUG && console.log(`a=setup`, utils.location.has('?a=setup') );
	utils.location.has('?a=setup') && document.setup
		&& !document.setup.doctype.insertAdjacentHTML('afterend',  // быстрые доктайпы
			stp.docTypes.map(DT => `<a href="javascript://" onclick="document.setup.doctype.value = (this.rel ? '' : this.title);" title='${DT.code}' >${DT.name}</a>` ).join(' '))
		&& stp.tmplMODE && !document.setup.offtext.insertAdjacentHTML('afterend',  // "Закрыт на техработы" добавить "Установка шаблона"
			` <a href="javascript://" onclick="document.setup.offtext.value = this.innerText;" class="replaceOFFTEXT">Установка шаблона</a>`);


	//// активация всех модулей для комментариев
stp.DEBUG && console.log(`a=soc_comments`, utils.location.has('?a=soc_comments') );
	utils.location.has('?a=soc_comments')
		&& window.mod_news && mod_news.insertAdjacentHTML('beforebegin',
			`<label class="activateALL">
				<input type="checkbox" onclick="for (CB of window['soc-comments-form'].comm_social ) !CB.disabled && (CB.checked = this.checked);" name="activateALL"> Вкл/выкл все</label>`);






	//// заменить "FTP пароль" и добавить 123qwe
stp.DEBUG && console.log(`a=ftppass`, utils.location.has('?a=ftppass') );
	utils.location.has('?a=ftppass') && stp.tmplMODE && document.setup.password1.insertAdjacentHTML('afterend',
		`&nbsp;<a href="javascript:" onclick="document.setup.password.value = document.setup.password1.value = this.textContent;">123qwe</a>`);


	//// ссылка на whois подле каждого домена, купленного через ПУ
stp.DEBUG && console.log(`a=mydomains`, utils.location.has('?a=mydomains') );
	utils.location.has('?a=mydomains') && document.querySelectorAll('td.udtr-domain').forEach(element => element.insertAdjacentHTML('beforeend',
			` &nbsp; <a href="javascript:" title="${element.textContent}" onclick="window.open('//dig.ua/search/'+this.title+'#content');">whois</a>`) );


	//// массовое удаление информеров
stp.DEBUG && console.log(`a=informers`, utils.location.has('?a=informers') );
	utils.location.has('?a=informers')
		&& document.querySelector('[id*=inf]') && (allTr = document.querySelectorAll('tr[id*=inf]'))
		&& !window['panel-cont'].querySelector('.contentBg').insertAdjacentHTML('beforeend',
			'<div flex-end >'
			+'<a onclick="if(confirm(`Вы подтверждаете данное действие?`)) for(TR of allTr) (INF=TR.id.match(/[0-9]+/).pop())&&(TR.children[2].innerHTML=`<img src=/.s/img/ma/m/i1.gif>`)&&_uPostForm(``,{url:`/panel/?a=informers&t=2&id=${INF}&ssid=${window.uCoz.ssid}`});" href="javascript:" data-deleteALLinf>Удалить все</a>'
			+'</div>'
		);


	//// фокусы в разных формах-полях
	// фокус в поле "Название раздела"/"Название форума"/"Название подфорума" при создании/редактировании раздела/форума/подфорума
stp.DEBUG && console.log(`a=forum && l=addcat|l=editcat|l=addforum|l=editforum|l=addsubforum|l=editsubforum`, utils.location.has('?a=forum'), `&&`, utils.location.has('l=addcat'), utils.location.has('l=editcat'), utils.location.has('l=addforum'), utils.location.has('l=editforum'), utils.location.has('l=addsubforum'), utils.location.has('l=editsubforum') );
	((utils.location.has('?a=forum') && (utils.location.has('l=addcat') || utils.location.has('l=editcat') || utils.location.has('l=addforum') || utils.location.has('l=editforum') || utils.location.has('l=addsubforum') || utils.location.has('l=editsubforum')))
	// фокус в поле названия фильтров
	|| utils.location.has('l=filter'))
		&& (document.setup && document.setup.name.focus() || document.addform && document.addform.name.focus());
	// фокус почтовых форм
stp.DEBUG && console.log(`a=mail && l=addform|l=editform|l=addfield|l=tmpl`, utils.location.has('?a=mail'), `&&`, utils.location.has('l=addform'), utils.location.has('l=editform'), utils.location.has('l=addfield'), utils.location.has('l=tmpl') );
	utils.location.has('?a=mail')
		// фокус в поле "Название почтовой формы" или "Название поля"
		&& (utils.location.has('l=addform') || utils.location.has('l=editform') || utils.location.has('l=addfield'))
		&& document.setup && document.setup.name && document.setup.name.focus()
		// фокус в поле "Шаблон почтовой формы"
		|| utils.location.has('l=tmpl')
		&& document.setup && document.setup.tmpl && document.setup.tmpl.focus();
	// фокус в поле "Конструктор шаблонов"
stp.DEBUG && console.log(`a=tmaker`, utils.location.has('?a=tmaker') );
	utils.location.has('?a=tmaker') && document.tform && document.tform.tmpl && document.tform.tmpl.focus();
	// фокус в поле "Быстрая замена участков шаблонов"
stp.DEBUG && console.log(`a=frep`, utils.location.has('?a=frep') );
	utils.location.has('?a=frep') && window.frm343 && frm343.from[0] && frm343.from[0].focus();




	//// action log process
stp.DEBUG && console.log(`a=log`, utils.location.has('?a=log') );
	if (utils.location.has('?a=log')) {
		cpl.log.initControls()  // add download log controls

		// TODO REFACTOR FILTER
		// (sep = document.querySelector('#panel-cont > div.xw-tl1')) && sep.insertAdjacentElement('afterend', sep.cloneNode(1))
		const LFMS = stp.logFilterModuleSelector.map(item => `<option value="${ item[0] }">${ item[1] }</option>`).join('')  // формирование списка опций селекторов фильтра
		const LFAS = stp.logFilterActionSelector.map(item => `<option value="${ item[0] }">${ item[1] }</option>`).join('')  // формирование списка опций селекторов фильтра
		// && (ALR = document.querySelectorAll('table.myTbl tr:not(:first-child)'))  // all log rows
		document.querySelector('.action-log.contentBg')?.insertAdjacentHTML('beforebegin', `<div id="fltrCntnr" >
			<a href="javascript:" onclick="mdl.value = ctn.value = 0; userGroup.value = ''; cpl.log.eachLines();" id="dropFILTER" title="Сбросить фильтр">Сброс</a> &bull;
			<select id="mdl" onchange="cpl.multiSelectEachLogLines(this);"><option value="0" disabled selected>Выбрать модуль</option>${LFMS}</select> &bull;
			<select id="ctn" onchange="cpl.multiSelectEachLogLines(this);"><option value="0" disabled selected>Выбрать действие</option>${LFAS}</select> &bull;
			<input placeholder="Пользователь/Группа" type="text" title="Регистрозависимый" id="userGroup"> &bull;
			${ [['193.109.246.211', 'vpn', ], ['109.122.62.126', 'офис', ], ].map(item => `<a href="javascript:" onclick="userGroup.value = this.dataset.ip; cpl.log.eachLines(this.dataset.ip)" data-ip="${item[0]}">${item[1]}</a>`).join('|') }
		</div>`)  //добавим блок для фильтров
		window.userGroup.addEventListener('keypress', event => (event.keyCode === 13) && cpl.log.eachLines(event.target.value))
	}

// console.log('cpl.CSS.styles', cpl.CSS.styles);

//// применение кастомных стилей
	utils.css.setStyle(cpl.getIcons(cpl.CSS.icons), 'cplCustomIcons');
stp.DEBUG && console.log(`mCustomScrollbar`, window.hasOwnProperty('mCustomScrollbar') );
stp.DEBUG && console.log(`jivo_config`, window.hasOwnProperty('jivo_config'), document.querySelector('[has-jivo]') );
stp.DEBUG && console.log(`cpl.CSS.styles`, cpl.CSS.styles );
	let isBWStyle = !!self.uCoz?.site.panel7
	utils.css.setStyle(
		[
			cpl.CSS.global,
			cpl.CSS.alt[ isBWStyle ],  // maybe needs to delete
			cpl.CSS.styles.filter(item => cpl.CSS[ item ]).map(type => [
				cpl.CSS[type].global,
				cpl.CSS[type][ isBWStyle ],
			].join('\n')).join('\n'),
		].join('\n'),
		'cplCustomCSS'
	);

})(
	window.cpl = {
		// get default custom font-icons
		getIcons : icons => icons && Object.keys(icons).map(icon => `.cpl-i-${ icon }:before{content:'\\${ icons[icon] }'; }`).join('\n') || '',

		// loader
		loader : {
			spinner : '<span id=cplLoader title=processing...><i class="fa cpl-i-spinner fa-spinner fa-pulse fa-3x fa-fw"></i></span>',
			set : target => target && (target.innerHTML = cpl.loader.spinner),
			add : target => target && target.insertAdjacentHTML('beforeend', cpl.loader.spinner),
			remove : target => (target || document).querySelectorAll('#cplLoader').forEach(element => element.remove()),
		},

		// get full module name TODO refactoring
		getModule : (MOD = 'si', MODULE = {nw:'news', pu:'publ', ld:'load', dr:'dir', bd:'board', bl:'blog', ph:'photo', sf:'stuff', fq:'faq', vi:'video', si:'site', ts:'tests', sh:'shop', }[MOD] ) => MODULE,
		// get short module name
		// TODO getMod : (MODULE = 'site', MOD = Object.assign({}, ...Object.entries(stp.mod).map(([a,b]) => ({[b]:a})))[MODULE] ) => MOD;

		// log filter
		log : {
			// get log data from server
			getData : () => fetch(location.pathname + location.search + '&json=1').then(response => response.json()),

			// set log getting controls
			initControls : (update, items ) => {
				items = [  // create control items
					{ href : 'javascript:;',  id : 'getLogDataButton',   onclick : 'cpl.log.build.prepareData(this)',  title : 'load all log data',  class : 'cpl-i-download',   },
					{ href : 'javascript:;',  id : 'downloadLogButton',  onclick : 'cpl.log.download.file(this)',      title : 'download log file',  class : 'cpl-i-file-text',  },
				].map(item => utils.template.render(cpl.log.tpl.controlItem, item)).join('\n');
				const logSelector = document.querySelector('.breadcrumbs select');  // get log selector
				// set wrapper & controls
				// window.getLogDataWrapper && getLogDataWrapper.remove();  //TODO DEBUG
				!window.getLogDataWrapper && logSelector.insertAdjacentHTML('beforebegin', utils.template.render(cpl.log.tpl.controls, {items}));
			},

			// do filter controls - TODO
			filter : {
				buildModuleSelector : () => {},  //TODO
				buildActionSelector : () => {},  //TODO
				reset : () => {},  //TODO
			},

			// try to download log...
			download : {
				// ...as file
				file : downloadButton => {
					!cpl.log.download.blob && cpl.log.getData().then(json => {  // get data and create Blob
						// update button status
						downloadButton.classList.add('cpl-logfile-ready');
						downloadButton.title = 'file ready to download';
						// create Blob & start download file
						cpl.log.download.prepareBlob(
							cpl.log.download.prepareData(json).join('\n'),
							'text/plain;charset=utf-8',
							logSelector.options[logSelector.selectedIndex].textContent,
						).click();
					});
					cpl.log.download.blob && cpl.log.download.a.click();  // just download file if Blob exists
				},
				prepareData : (json ) => json.reverse().map(item => utils.template.render(cpl.log.tpl.fileRow, item)),
				prepareBlob : (data, type, name ) => {
					cpl.log.download.blob = new Blob([data], {type});
					cpl.log.download.a = document.createElement('a');
					[cpl.log.download.a.href, cpl.log.download.a.target, cpl.log.download.a.download]
						= [(window.URL || window.webkitURL).createObjectURL(cpl.log.download.blob), '_blank', name];
					return cpl.log.download.a;
				},
			},

			// load all logs
			build : {
				list : (json ) => {
					data = json.map(item => utils.template.render(cpl.log.tpl.tableRow, item));
					cpl.loader.remove();  // remove loader
					logTable.firstElementChild.insertAdjacentHTML('beforeend', data.join(''));  // set log data to table
				},
				prepareData : (startButton ) => {
					startButton.remove();  // remove start button
					getLogDataWrapper.insertAdjacentHTML('afterbegin', cpl.loader.spinner);  // cpl.loader.add(getLogDataWrapper);  //add loader
					cpl.log.build.prepareTable();  // prepare table
					window.logPaging = document.querySelector('.paging-wrapper');  // get paging
					// get log data from (session storage(TODO) || server)
					cpl.log.getData().then(json => {
						cpl.log.build.list(json);  // build log table
						logPaging && logPaging.remove();  // remove paging
					});
				},
				prepareTable : (table = document.querySelector('.myTbl') ) => {
					table.id = 'logTable';
					logHeader = table.querySelector('tr:first-child').cloneNode(1);  // save original header
					table.innerHTML = '';  // clear table
					table.insertAdjacentHTML('afterbegin', logHeader.innerHTML);  // append header
					// set loader
					!window.loaderWrapper && table.insertAdjacentHTML('afterend', '<center id=loaderWrapper></center>')
					cpl.loader.set(loaderWrapper);
				},
			},

			// some logs templates
			tpl : {
				controls : `<div id=getLogDataWrapper class=cpl-log-controls-wrapper>{{items}}</div> `,
				controlItem : `<a ??href=href="{{href}}"?? ??id=id={{id}}?? ??onclick=onclick={{onclick}}?? ??title=title="{{title}}"??><i class="fa ??class={{class}}??" aria-hidden="true"></i></a>`,
				tableRow : `<tr><td>{{time}} <td>{{action}} <td>{{user}} ??group=({{group}})?? ??ip=<td>{{ip}}?? <td>{{comment}} </tr>`,
				fileRow : `{{time}}\t{{action}}\t{{user}}??group= ({{group}})????ip=\t{{ip}}??\t{{comment}}`,
			},

			// show log table header after filter do
			showHeader : (logHeader = document.querySelector('.myTbl tr:first-child') ) => !!logHeader && (logHeader.style.display = ''),

			// loop on log lines
			eachLines : (searchText = '', ALR = document.querySelectorAll('table.myTbl tr') ) => {
				if (searchText) for (let ITEM of ALR ) (ITEM.style.display = ITEM.textContent.trim().includes(searchText, 12) ? '' : 'none');
				cpl.log.showHeader();
			},
		},
		// do filter controls - TODO REFACTOR
		multiSelectEachLogLines : (SELECT, SIBLING = window[{mdl:'ctn', ctn:'mdl' }[SELECT.id]], ALR = document.querySelectorAll('table.myTbl tr') ) => {
			(+SIBLING.value == 0) && cpl.log.eachLines(SELECT.value);
			if (+SIBLING.value != 0 ) for (let ITEM of ALR ) (ITEM.style.display = (ITEM.textContent.includes(SELECT.value, 12) && ITEM.textContent.includes(SIBLING.value, 12) ) ? '' : 'none');
			cpl.log.showHeader();
		},


		getLink : link => tpl.link.render(link),

		// ROUTE HANDLERS
		// ?

		// system CSS
		CSS    : {
			styles : [],

			icons : {
				close : 'f00d',

				style : 'f121',
				filter : 'f0b0',
				entries : 'f0ca',
				setup : 'f013',
				tmpl : 'f1c5',

				users : 'f0c0',
				mail : 'f0e0',
				stat : 'f200',
				sharing : 'f1e0',
				'chart-bar' : 'f080',
				'chart-line' : 'f201',

				'file-text' : 'f0f6',
				download : 'f0ed',
				spinner : 'f110',
				extlink : 'f08e',
			},

			log : {
				global : `/* action log */
					.cpl-log-controls-wrapper{margin:0 .75em 0 0; }
					.cpl-logfile-ready .fa{color:green; font-weight:bold; }
					.myTbl td{width:auto; }
					.myTbl td:first-child{white-space:nowrap; }
					.myTbl td:nth-of-type(2){min-width:175px; }
					.myTbl td:nth-child(3){text-align:center; }`,
				true : ``,
				false : ``,
			},

			global    : `/* custom global CSS */
.fa-pulse{-webkit-animation:fa-spin 1s infinite steps(8); animation:fa-spin 1s infinite steps(8)}
@-webkit-keyframes fa-spin{
	0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}
	100%{-webkit-transform:rotate(359deg);transform:rotate(359deg)}
}
@keyframes fa-spin{
	0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}
	100%{-webkit-transform:rotate(359deg);transform:rotate(359deg)}
}

a.fa.cpl-i-extlink { text-decoration:none; }

/* modules menu */
.MmenuOut { position:relative; }
.MmenuOut :is( a.mml ) { max-width:unset; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) { text-align:end; align-self:center; flex-shrink:0; width:fit-content; display:flex; align-items:center; gap:.125rem; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) a:has( i.fa ) { display:flex; align-items:center; justify-content:center; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) .mod-cat { order:-1; width:17px; min-width:17px; position:absolute; left:2px; height:17px; }
.MmenuOut :is( i.fa ) { width:auto; height:auto; }




/* TODO - FIXME refactor */

.cpl-i-style:before{font-weight:bold; }

.fa-times:before{content:'\\f00d'; }
.fa-pencil:before{content:'\\f040'; }

a.noun.aliasLink{position:relative; padding-right:30px; }
a.noun.aliasLink:after{content:'»'; margin:-6px 0 0 4px; position:absolute; font:bold 2em/1 sans-serif; color:green; }

input[name="doctype"]{width:100%; padding-right:20px; }
a.doctypeCLEAR{position:absolute; right:22px; margin:-19px 0 0; text-decoration:none; }

label.activateALL{display:block; font-weight:bold; margin:0 0 4px; }

.cpl-users.cpl-add textarea[name="signature"],
.cpl-users.cpl-add input[name="avatar"]{width:350px!important; }
.addNEW{position:absolute; margin:36px 20px 0; right:2px; }

tr.cpl-fly-button, div.cpl-fly-button{display:flex; position:fixed; bottom:0; right:0; left:0; padding:4px 1px 5px; justify-content:flex-end; align-items:center; z-index:1; box-sizing:border-box; }
tr.cpl-fly-button > td{padding:1px 3px; width:${!!document.querySelector('[has-jivo]') ? '425px' : 'auto'}; }

.udtr-wrap{padding:10px 15px 0!important; }
.udtr-wrap > table{margin:0 auto; }
.udtr-wrap td{padding:7px 15px 8px!important; }

/*cсылка на RSS модуля*/
.rssLINK{float:right; display:inline-block; }
.rssLINK a{color:blue; }

/*SEO-url переключатель*/
[data-prefix]{position:absolute; right:22px; margin:2px 0; }

/*ссылка удаления информеров*/
[data-deleteALLinf]{float:right; font-weight:bold; text-decoration:none!important; color:brown!important; margin:3px 4px 0; }

/* TEMPLATES */
.tmpl i.fa{display:inline-block; background:#444; font-size:19px; color:#fff!important; width:auto; height:auto;
	cursor:pointer; padding:1px 3px; margin:-5px 6px 0 4px; }
.tmpl i.fa::before{opacity:1!important; }

/* Фильтр лога действий */
#dropFILTER{}
#buildALLpage{}
#fltrCntnr { display:flex; justify-content:center; align-items:center; gap:.25rem; }
#fltrCntnr select option[selected]{font:bold italic 9pt sans-serif!important; color:#930; }

.cpl-users .myTbl td[align=center]{white-space:nowrap; }
`,
			alt       : {
				true     : `/* custom newBW CSS */
a.noun.aliasLink:after{margin:-4px 0 0 10px; right:7px; }

.fa.cpl-i-close, a:hover .fa.cpl-i-close { background:none; }

[container]dashboard-container { max-width:unset; }

/* modules menu */
.MmenuOut :is( .mmenuItem ):before { flex-shrink:0; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) { gap:.25rem; margin:0 .5rem; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) .mod-cat { width:1.5rem; position:absolute; left:.25rem; height:1.5rem; }
.MmenuOut :is( .cpl-menu-icon-wrapper ) i.fa { font-size:1rem; line-height:1rem; padding:0; }
.MmenuOut.MmenuSelected :is( a.mmla ) { color:#304e7b; }



.tmpl i.fa{padding:4px 3px; }

tr.cpl-fly-button, div.cpl-fly-button{padding:7px 3px 8px; background:#2c2e32; }
#panel-cont.buttonFixed{padding-bottom:40px; }


form#tmplFrm34 > table:last-of-type{margin:10px 0 -17px; }
`,
				false    : `/* custom oldCP CSS */
@font-face{
	font-family:'FontAwesome'; font-weight:normal; font-style:normal;
	src:url('/.s/src/font/fontawesome-webfont.eot');
	src:url('/.s/src/font/fontawesome-webfont.eot?#iefix') format('embedded-opentype'),
		url('/.s/src/font/fontawesome-webfont.woff2') format('woff2'),
		url('/.s/src/font/fontawesome-webfont.woff') format('woff'),
		url('/.s/src/font/fontawesome-webfont.ttf') format('truetype'),
		url('/.s/src/font/fontawesome-webfont.svg#fontawesomeregular') format('svg');
}
.fa{display:inline-block; font:normal normal normal .9rem/1 FontAwesome; text-rendering:auto; color:#444;
	-webkit-font-smoothing:antialiased; -moz-osx-font-smoothing:grayscale; transform:translate(0,0); cursor:pointer; }

[content] .MmenuOut{position:relative; }


#panel-cont.buttonFixed{padding-bottom:38px; }
#ufm_uploadform > table, #ufm_filestable{width:100%; }

/*
[data-tab]{background:#e1e9f5; cursor:pointer; background:linear-gradient(#f3f6fb, #d4e0f1); padding:4px 10px 6px; margin:-4px 0 0; display:inline-block; }
[data-tab].active{font-weight:bold; }
[data-tab]:hover{background:linear-gradient(#f3f6fb, #99bbe8); }
[tabfooter]{border:none; border-top:1px solid #99bbe8; border-bottom:1px solid #99bbe8; height:5px; background:#d3e2f5; background:linear-gradient(#d3e2f5, #d3e2f5); margin:0 -4px 0; }
[tabset]:not(.active){display:none; }
*/

#panel-cont .servMenuList i.fa:before,
#panel-cont .topBlockM i.fa:before,
#panel-cont #shop-cp-cont i.fa:before, #panel-cont #shop-cp-cont a.fa:before, /* shop */
#panel-cont .del_mod i.fa:before, /* ya feeds */
#panel-cont .entry-filters i.fa:before, /* filters */
#panel-cont .no-b i.fa:before, /* video cats */
#panel-cont [class^=catGr] i.fa:before, #panel-cont [class^=catCtP] i.fa:before, /* forums */
#panel-cont .myBdTop.myBdRight.myBdBottom.myBdLeft i.fa:before, /* users groups */
#panel-cont.awards i.fa:before, /* awards */
#filemanager .ufm-table i.fa:before,
/* #panel-cont .myTbl i.fa:not([class*="cpl-i"]):before, */
#panel-cont .cont-table i.fa:before{opacity:0; }
` + (window._rtl ? `
	/* RTL addition */
#isInMo .mnSub{margin:-8px 20px 0 0; float:left; }
#isInMo .mmenuItem{padding:1px 21px 4px 0px!important; }
` : ''),
			}
		},
	},
	new Date(), new Date().getDate(), (new Date().getMonth() + 1), new Date().getFullYear(),
	// SETUP
	{
		//// DEBUG
		// DEBUG : 1,
		//// SETTINGS
		thisSite         : ( utils.convert.user2site( self.uCoz?.site.id ) || '' ),  // адрес текущего сайта
		ssID             : self.uCoz?.ssid || 0,
		testMODE         : 0,  // режим тестировщика
		tmplMODE         : 0,  // режим установки шаблонов
		autoenableMODULE : 0,  // автоактивация модулей
		hideBanner       : 1,  // удалять рекламный баннер в ПУ
		// setupTabs        : 1,  // вкладки в настройках модулей - TODO
		// layer            : +document.head.querySelector('link[href*=layer]').getAttribute('href').match(/\d/),  // номер текущего стиля ПУ
		// быстрый доктайп в общих настройках
		docTypes            : [
			{ name : 'html5',       code : '<!DOCTYPE html>' },
			{ name : 'html 4.01 S', code : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">' },
			{ name : 'html 4.01',   code : '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">' },
			{ name : 'xhtml 1.0 S', code : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' },
			{ name : 'xhtml 1.0',   code : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' },
			{ name : 'xhtml 1.1',   code : '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">' },
			{ name : '&#10060;',    code : `Очистить' class="doctypeCLEAR" rel='1` },
		],
		// селекторы фильтра лога действий
		logFilterModuleSelector : [
			[ 'User',      'Пользователи' ],  [ 'index',  'Страницы'          ],
			[ 'news',      'Новости'      ],  [ 'forum',  'Форум'             ],
			[ 'publ',      'Статьи'       ],  [ '/load',  'Файлы'             ],
			[ 'dir',       'Сайты'        ],  [ 'board',  'Объявления'        ],
			[ 'blog',      'Блог'         ],  [ 'photo',  'Фотоальбом'        ],
			[ 'stuff',     'Игры'         ],  [ 'gb',     'Гостевая'          ],
			[ 'faq',       'FAQ'          ],  [ 'search', 'Поиск'             ],
			[ 'tests',     'Тесты'        ],  [ 'chat',   'Мини-чат'          ],
			[ 'poll',      'Опросы'       ],  [ 'stati',  'Статистика'        ],
			[ 'shop',      'Магазин'      ],  [ 'video',  'Видео'             ],
			[ 'crosspost', 'Постинг'      ],  [ 'a=fm',   'Файловый менеджер' ],
		],
		logFilterActionSelector : [
			[ 'adding',   'Добавление'     ],  [ 'saving',      'Сохранение'   ],
			[ 'deleting', 'Удаление'       ],  [ 'calculating', 'Калькуляция'  ],
			[ 'changing', 'Изменение'      ],  [ 'moving',      'Перемещение'  ],
			[ 'export',   'Экспорт'        ],  [ 'uploading',   'Загрузка'     ],
			[ 'copying',  'Копирование'    ],  [ 'updat',       'Обновление'   ],
			[ 'creating', 'Создание'       ],  [ 'activation',  'Активация'    ],
			[ 'setting',  'Установка'      ],  [ 'clearing',    'Очистка'      ],
			[ 'hiding',   'Скрытие'        ],  [ 'switching',   'Переключение' ],
			[ 'manage',   'Управление'     ],  [ 'login',       'Вход'         ],
			[ 'editing',  'Редактирование' ],  [ 'logout',      'Выход'        ],
		],
	},
	window.tpl = {
		tab : `<span class="{{active}}" data-tab="{{name}}" onclick=cpl.switch(this);>{{name}}</span>`,
		link : `<a href="{{href}}" {{attrs}} class="{{class}}" target="{{target}}" title="{{title}}">{{name}}</a>`,
	},
	self.utils
);





// migrate to Class
// TODO
class ControlPanelLite {
	// debug = 1

	constructor(utils) {
		this.utils = utils
		this.initUrlParams()

		this.debug && console.log( `this.skippedLocation()`, this.skippedLocation() )
		// this.debug && console.log( `this.URL.a`, this.URL.a )

		if (this.skippedLocation()) return

		this.debug && console.log( `this`, this )
		this.debug && console.log( `location`, location )

		this.identifyPage()
		this.initStyles()
		this.initPageHandler()
	}

	// cpl utils

	initUrlParams() {
		this.URL = Object.fromEntries( new URLSearchParams(location.search.replace(/;/g, '&')) )
	}

	skippedLocation() {
		switch (this.URL.a) {
			case 'tt' :
			case 'smiles' :
			case 'moders' :
				return true; break;
			default : return false
		}
	}

	identifyPage() {
		self['panel-cont']?.classList.add(`cpl-${ this.URL.a }`)
		this.URL.l && self['panel-cont']?.classList.add(`cpl-${ [ this.URL.a, this.URL.l ].join('-') }`)
		// !!key && cpl.CSS.styles.push(key);
	}

	initStyles() {
		// this.utils.css.set...
	}

	initPageHandler() {
		this.globalHandler()

		// prepare handler name
		let handlerName = this.URL.l
			? `${ this.URL.a + this.#capitalizeFirst(this.URL.l) }Handler`
			: `${ this.URL.a || 'cp' }Handler`

		this.debug && console.log( `handler`, handlerName, this[ handlerName ] )

		// run handler
		if ( this[ handlerName ] ) this[ handlerName ]()
	}

	#capitalizeFirst(string) {
		return !!string ? string[0].toUpperCase() + string.slice(1) : ''
	}

	#createHeaderSidebar( params = {} ) {
		// check if exists
		const headerSidebarElement = document.querySelector('.page-header-sidebar')

		// add/update
		if ( headerSidebarElement ) {
			headerSidebarElement.id = 'cplHeaderSidebar'
		} else {
			document.querySelector('.page-header-content')?.insertAdjacentHTML( 'afterend', this.template.headerSidebar.render( params ) )
		}

		return
	}

	#updatePageIcons() {
		Array.from( arguments ).forEach( icon => {
			if ( !self[ icon ] ) document.querySelector('body > svg[style]')?.insertAdjacentHTML( 'beforeend', this.icons[ icon ] )
			else console.info( `Icon '${ icon }' already exists! You can remove it from current.userscript.` )
		} )
	}

	getLink(link) {
		return this.template.link.render(link)
	}

	// page handlers

	globalHandler() {
		this.debug && console.log( `control panel GLOBAL handler` )

		// set sidebar modules button
		// this.#setModsButtonHandler()  //TODO

		// remove cp banner
		document.querySelector('.cp-banner-wrapper')?.remove()
	}

	#setModsButtonHandler() {
		if ( !self.isInMo ) return

		const modIconsData = {
			//      0 - categories,           1 - styles/users                    2 - filters          3 - entries/forums    4 - settings                   5 - view/other templates
			// if ARRAY: 0 - url, 1 - custom class
			mmus : [ 'a=users&l=groups',      [ 'a=users&l=find', 'users' ],      0,                   0,                    'a=users&l=setup',             'a=tmpl&m=4&t=5'                      ],
			mmsi : [ 0,                       'a=tmpl&m=3&t=3',                   0,                   'a=entries&m=si',     'a=setup',                     'a=tmpl&m=2&t=0'                      ],
			mmnw : [ 'a=cats&m=nw',           0,                                  'a=news&l=filter',   'a=entries&m=nw',     'a=news&l=setup',              'a=tmpl&m=6&t=3'                      ],
			mmfr : [ 'a=forum&l=order',       0,                                  0,                   'a=forum&l=forums',   'a=forum&l=setup',             'a=tmpl&m=8&t=2'                      ],
			mmpu : [ 'a=cats&m=pu',           0,                                  'a=publ&l=filter',   'a=entries&m=pu',     'a=publ&l=setup',              'a=tmpl&m=10&t=7'                     ],
			mmld : [ 'a=cats&m=ld',           0,                                  'a=load&l=filter',   'a=entries&m=ld',     'a=load&l=setup',              'a=tmpl&m=11&t=7'                     ],
			mmdr : [ 'a=cats&m=dr',           0,                                  'a=dir&l=filter',    'a=entries&m=dr',     'a=dir&l=setup',               'a=tmpl&m=12&t=7'                     ],
			mmbd : [ 'a=cats&m=bd',           0,                                  'a=board&l=filter',  'a=entries&m=bd',     'a=board&l=setup',             'a=tmpl&m=13&t=7'                     ],
			mmbl : [ 'a=cats&m=bl',           0,                                  'a=blog&l=filter',   'a=entries&m=bl',     'a=blog&l=setup',              'a=tmpl&m=7&t=4'                      ],
			mmph : [ 'a=cats&m=ph',           'a=tmpl&m=9&t=10',                  'a=photo&l=filter',  'a=entries&m=ph',     'a=photo&l=setup',             'a=tmpl&m=9&t=8'                      ],
			mmsf : [ 'a=cats&m=sf',           0,                                  'a=stuff&l=filter',  'a=entries&m=sf',     'a=stuff&l=setup',             'a=tmpl&m=21&t=7'                     ],
			mmgb : [ 0,                       0,                                  0,                   0,                    'a=gb&l=setup',                'a=tmpl&m=15&t=2'                     ],
			mmfq : [ 'a=cats&m=fq',           0,                                  0,                   'a=entries&m=fq',     'a=faq&l=setup',               'a=tmpl&m=14&t=2'                     ],
			mmse : [ 0,                       0,                                  0,                   0,                    'a=search&l=setup',            'a=tmpl&m=19&t=2'                     ],
			mmts : [ 'a=cats&m=ts',           0,                                  0,                   'a=entries&m=ts',     'a=tests&l=setup',             'a=tmpl&m=18&t=1'                     ],
			mmmc : [ 0,                       0,                                  0,                   0,                    'a=mchat&l=setup',             'a=tmpl&m=17&t=1'                     ],
			mmmf : [ 0,                       0,                                  0,                   0,                    [ 'a=mail&l=forms', 'mail' ],  'a=mail&l=tmpl'                       ],
			mmpo : [ 0,                       0,                                  0,                   'a=poll&l=polls',     'a=poll&l=setup',              'a=tmpl&m=16&t=1'                     ],
			mmst : [ 0,                       0,                                  0,                   0,                    'a=stat&l=setup',              [ 'a=stat&z=z', 'stat' ]              ],
			mmsh : [ 'a=shop&l=cats',         'a=tmpl&m=20&t=9',                  0,                   'a=shop&l=goods',     'a=shop&l=setup',              'a=tmpl&m=20&t=3'                     ],
			mmvi : [ 'a=video&l=cat_manage',  'a=tmpl&m=22&t=11',                 'a=video&l=filter',  'a=video&l=entries',  'a=video&l=setup',             'a=tmpl&m=22&t=8'                     ],
			mmcs : [ 'a=crosspost&l=setup',   0,                                  0,                   0,                    'a=crosspost&l=settings',      [ 'a=crosspost&l=status', 'sharing' ] ],
			mmso : [ 'a=seo&l=monitoring',    [ 'a=seo&l=wizard', 'chart-bar' ],  0,                   0,                    'a=seo&l=settings',            [ 'a=seo&l=redirects', 'chart-line' ] ],
		}

		for ( let [ key, value ] of Object.entries(modIconsData) ) {
			self.isInMo?.querySelector(`.mmenuItem.${ key }`)?.insertAdjacentHTML('beforeend', `<span class=cpl-menu-icon-wrapper >${ this.#buildModButtons(value).join('') }</span>`)
		}
	}

	#buildModButtons(buttonsData = []) {
		const modIconsClasses = [ 0, 'style', 'filter', 'entries', 'setup', 'tmpl' ]
		const buttonsHtml = buttonsData.map(function(item, index) {
			const pdaParam = (index === 4) && self.location?.search?.includes('pda=1') ? `&pda=1` : ''
			const buttonFromStringData = self.isString(item) ? `<a ${ !index ? 'class="mod-cat"' : '' } href="?${ item + pdaParam }"><i class="fa cpl-i-${ modIconsClasses[ index ] }"></i></a>` : null
			const buttonFromArrayData = self.isArray(item) ? `<a href="?${ item.join('"><i class="fa cpl-i-') }"></i></a>` : null
			return buttonFromStringData || buttonFromArrayData || undefined
		})
		return buttonsHtml
	}

	cpHandler() { // control panel main page handler
		// console.log( `control panel MAIN handler` )

		// domain manage links
		const domainManageLinks = [
			{ value:'Управление доменами', href:'?a=domain_transfer', icon:'#p7_website_icon' },
		]
		const dashboardCardTargertList = document.querySelector('.dashboard-card [href="?a=mydomains"]')?.closest('ul')

		dashboardCardTargertList?.insertAdjacentHTML( 'afterbegin', domainManageLinks.map( link => this.template.dashboardCardItem.render( link ) ).join('') )
	}

	// usersHandler() { }

	usersAddHandler() {
		this.#createHeaderSidebar()

		const fillUserDataButton = document.createElement('button')
		Object.assign( fillUserDataButton, { className:'ubtn-default light-btn', textContent:'Fill user data', onclick:this.#usersAddHandler_fillUserData.bind(this) } )

		self.cplHeaderSidebar?.insertAdjacentElement( 'beforeend', fillUserDataButton )
	}

	#usersAddHandler_fillUserData(event) {
		// this.debug && console.log( `control panel FILLUSERDATA handler` )
		const userData = {
			user      : 'usupport',
			password  : 'rdflhjajybz',  // password - \квадрофония\
			gid       : 4,
			name      : 'Support User',
			email     : 'usupport@usupport.site',
			home_page : 'https://usupport.site/',
			country   : 212,
			state     : 'Che',
			city      : 'Cherkasy',
			by        : (new Date().getFullYear() - 25),
			bm        : (new Date().getMonth() + 1),
			bd        : new Date().getDate(),
			gender    : 1,
			signature : 'Test user for uCoz support',
			avatar    : '/.s/img/err/404-logo.png',  // '//logo.ucoz.com/img/logom.png',
		}

		Object.entries(userData).forEach( ([ name, value ]) => { document.setup[ name ].value = value } )
		Array.from( document.setup.elements.opt ).filter( (element, index) => (index == 1 || index == 4) ).forEach( element => { element.checked = false } )
	}

	usersFindHandler() {
		this.#updatePageIcons( 'p7_gear_icon' )

		// add find user links to find form
		this.#usersFindHandler_FindUser( document.getElementById('frm821') )

		// add UID remover button
		this.#usersFindHandler_SetAllowDeleteUIDUsersButton( document.querySelector('.users-table .u7-table__header:last-child') )

		// add groups settings button
		this.#usersFindHandler_SetGroupsPermissionsButton()
	}

	#usersFindHandler_FindUser( findUserForm ) {
		// create/update sidebar
		this.#createHeaderSidebar()

		// update header sidbar
		const addNewUserLink = document.createElement('a')
		Object.assign( addNewUserLink, { href:'?a=users&l=add', className:'ubtn-link', textContent:'Add user' } )

		self.cplHeaderSidebar?.insertAdjacentElement( 'beforeend', addNewUserLink )

		// create wrapper
		const findUserLinksWrapper = document.createElement('div')

		// create users links
		;[ 'usupport' ].map(user => {
			let userLink = document.createElement('a')
			Object.assign(userLink, { href:'javascript:;', textContent:user, title:'Find user', onclick:this.#usersFindHandler_FindUserClickHandler.bind(this, userLink, findUserForm) })

			findUserLinksWrapper.insertAdjacentElement('afterbegin', userLink)
		})

		// append wrapper
		findUserForm?.insertAdjacentElement('afterbegin', findUserLinksWrapper)
		findUserForm?.classList.add('flex-align-center')
	}

	#usersFindHandler_FindUserClickHandler(link, form) {
		if ( !form ) return

		form.u.value = link.innerText
		form.submit()
	}

	#usersFindHandler_SetAllowDeleteUIDUsersButton( headerCell ) {
		if ( !headerCell ) return

		// set button
		let allowDeleteUIDButton = document.createElement('button')
		Object.assign(allowDeleteUIDButton, {
			type      : 'button',
			innerHTML : this.template.buttonIconImage.render( { icon:'#p7_cross_icon' } ),
			title     : 'Enable UID users deletion',
			className : 'cp-icon-btn cp-icon-btn--inline danger-hover',
			onclick   : this.#usersFindHandler_AllowDeleteUIDUsersClickHandler.bind(this),
		})

		headerCell?.insertAdjacentElement( 'beforeend', allowDeleteUIDButton )
	}

	#usersFindHandler_AllowDeleteUIDUsersClickHandler(event) {
		if ( !confirm('Do you really want to enable uid users deletion?') ) return

		// update inactive delete buttons
		document.querySelectorAll('.users-table .u7-table__row .cp-icon-btn.danger-hover[onclick*=".helper("]').forEach( ( element, idx, arr ) => {
			let row = element.closest('tr')
			let userId = +element.id.match(/(\d+)/).pop()
			let userLogin = row.children[1].firstChild.text.trim()

			element.setAttribute( 'onclick', `uCoz.utils.deleteItem({l:'delete', u:'${ userId }', uuser:'${ !userId ? userLogin : '' }'})` )
		})
	}

	#usersFindHandler_SetGroupsPermissionsButton() {
		// set group permissions link
		document.querySelectorAll('.users-table .u7-table__cell select').forEach( select => {
			const groupRightsButton = this.getLink({ href:`?a=users&l=groups&t=2&id=${ select.value }`, class:'cp-btn-link cpl-userlist-gear', title:'Edit group properties or permissions', name:this.template.buttonIconImage.render( { icon:'#p7_gear_icon' } ) })
			const target = select.closest('td')

			target?.setAttribute( 'nowrap', 'nowrap' )
			target?.insertAdjacentHTML( 'beforeend', groupRightsButton )
		} )
	}

	// templates

	template = {
		buttonIconImage : `<svg class="svg-icon-x"><use href="{{icon}}"></use></svg>`,
		link : `<a href="{{href}}" {{attrs}} class="{{class}}" target="{{target}}" title="{{title}}">{{name}}</a>`,
		dashboardCardItem : `<li><a class="setting-link" href="{{href}}"><svg class="setting-icon"><use xlink:href="{{icon}}"></use></svg><span class="setting-name">{{value}}</span></a></li>`,
		headerSidebar : `<div id="cplHeaderSidebar" class="page-header-sidebar"></div>`,
	}

	// icons
	icons = {
		p7_gear_icon : `<symbol id="p7_gear_icon" viewBox="0 0 24 24" fill="none"><path d="M14.1213 9.87868C15.2929 11.0503 15.2929 12.9497 14.1213 14.1213C12.9497 15.2929 11.0503 15.2929 9.87868 14.1213C8.70711 12.9497 8.70711 11.0503 9.87868 9.87868C11.0503 8.70711 12.9497 8.70711 14.1213 9.87868Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M15.2043 3.13523L15.8603 3.35349C16.4611 3.55375 16.8672 4.11627 16.8672 4.74966V5.69133C16.8672 6.49236 17.5063 7.146 18.3074 7.16288L19.2502 7.18313C19.7959 7.19438 20.291 7.50827 20.5351 7.99654L20.8445 8.61531C21.128 9.18233 21.0167 9.86636 20.5689 10.3141L19.9028 10.9801C19.3369 11.546 19.3267 12.4607 19.8803 13.039L20.5329 13.7208C20.9109 14.1156 21.0392 14.686 20.8659 15.2047L20.6476 15.8606C20.4473 16.4614 19.8848 16.8675 19.2514 16.8675H18.3096C17.5086 16.8675 16.8549 17.5065 16.838 18.3076L16.8177 19.2503C16.8065 19.796 16.4926 20.291 16.0043 20.5351L15.3855 20.8445C14.8184 21.128 14.1343 21.0167 13.6865 20.5689L13.0205 19.9029C12.4545 19.337 11.5398 19.3268 10.9615 19.8804L10.2797 20.5329C9.88479 20.9109 9.31436 21.0392 8.79568 20.8659L8.13974 20.6476C7.53893 20.4474 7.13276 19.8849 7.13276 19.2515V18.3098C7.13276 17.5088 6.4937 16.8551 5.69262 16.8382L4.74977 16.818C4.20409 16.8067 3.70905 16.4929 3.4649 16.0046L3.15549 15.3858C2.87196 14.8188 2.98335 14.1348 3.43114 13.687L4.09721 13.021C4.66314 12.4551 4.67327 11.5404 4.11971 10.9621L3.46715 10.2804C3.08798 9.88436 2.95972 9.31284 3.13299 8.79532L3.35126 8.13942C3.55153 7.53864 4.11409 7.1325 4.74752 7.1325H5.68924C6.49032 7.1325 7.14401 6.49348 7.16089 5.69245L7.18114 4.74966C7.19464 4.20402 7.50743 3.709 7.99572 3.46487L8.61454 3.15548C9.18159 2.87197 9.86566 2.98335 10.3135 3.43112L10.9795 4.09714C11.5455 4.66304 12.4602 4.67316 13.0385 4.11964L13.7203 3.46712C14.1152 3.09023 14.6868 2.96197 15.2043 3.13523V3.13523Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path></symbol>`,
	}

	// predefined data
}

new ControlPanelLite(self.utils)
